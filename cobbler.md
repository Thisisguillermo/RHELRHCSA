# Cobbler

*"Cobbler is a Linux installation server that allows for rapid setup of network installation environments. It glues together and automates many associated Linux tasks so you do not have to hop between many various commands and applications when deploying new systems, and, in some cases, changing existing ones. Cobbler can help with provisioning, managing DNS and DHCP, package updates, power management, configuration management orchestration, and much more."*

https://cobbler.github.io/

## Installation

```
yum install epel-release -y

yum install cobbler cobbler-web dnsmasq syslinux pykickstart xinetd -y

systemctl start cobblerd ; systemctl enable cobblerd

systemctl start httpd ; systemctl enable httpd

firewall-cmd --add-port=80/tcp --permanent

firewall-cmd --add-port=443/tcp --permanent

firewall-cmd --add-service=dhcp --permanent

firewall-cmd --add-port=69/tcp --permanent

firewall-cmd --add-port=69/udp --permanent

firewall-cmd --add-port=4011/udp --permanent

firewall-cmd --reload

```

## Generate the encrypted root password

```
[root@cloudworld ~]# openssl passwd -1
Password:
Verifying - Password:
$1$j9/aR8Et$uovwBsGM.cLGcwR.Nf7Qq0
 [root@cloudworld ~]#
```

## Update this encrypted string in settings file ‘/etc/cobbler/settings‘

Update this encrypted string in settings file ‘/etc/cobbler/settings‘ under the parameter ‘default_password_crypted‘ and also enable Cobbler’s DHCP, DNS,PXE and TFTP feature by changing the parameter  value from 0 to 1.

Specify the ip address of your TFTP server in ‘next_server’ parameter and Cobbler’s Server ip address in ‘server’ parameter.

```
Update this encrypted string in settings file ‘/etc/cobbler/settings‘ under the parameter ‘default_password_crypted‘ and also enable Cobbler’s DHCP, DNS,PXE and TFTP feature by changing the parameter  value from 0 to 1.

Specify the ip address of your TFTP server in ‘next_server’ parameter and Cobbler’s Server ip address in ‘server’ parameter.
```

## Update ‘/etc/cobbler/dhcp.template’ and  ‘/etc/cobbler/dnsmasq.template’ file

```
# ******************************************************************
# Cobbler managed dhcpd.conf file
#
# generated from cobbler dhcp.conf template ($date)
# Do NOT make changes to /etc/dhcpd.conf. Instead, make your changes
# in /etc/cobbler/dhcp.template, as /etc/dhcpd.conf will be
# overwritten.
#
# ******************************************************************

ddns-update-style interim;

allow booting;
allow bootp;

ignore client-updates;
set vendorclass = option vendor-class-identifier;

option pxe-system-type code 93 = unsigned integer 16;

subnet 192.168.122.0 netmask 255.255.255.0 {
     option routers             192.168.122.229;
     option domain-name-servers 192.168.122.229;
     option subnet-mask         255.255.255.0;
     range dynamic-bootp        192.168.122.230 192.168.122.260;
     default-lease-time         21600;
     max-lease-time             43200;
     next-server                $next_server;
```
### Update the IP address range for pxe clients in the file ‘/etc/cobbler/dnsmasq.template’
```
[root@cloudworld ~]# vi /etc/cobbler/dnsmasq.template
dhcp-range=192.168.122.230,192.168.122.260
```

## Restart the Cobbler and xinetd service and sync these changes to cobbler.

```
systemctl restart cobblerd
systemctl restart xinetd ; systemctl enable xinetd
cobbler check ; cobbler sync
```

## Checking for cobbler issues

```
cobbler check
```
